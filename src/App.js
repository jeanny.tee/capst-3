import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Undefined from './pages/Products'
import ProductView from './components/ProductView';
import Cart from './pages/Cart';
import Orders from './pages/Orders';

import {useState, useEffect} from 'react';

import { UserProvider } from './UserContext';

import './App.css';

import {BrowserRouter, Route, Routes} from 'react-router-dom';
import Products from './pages/Products';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null
  });


  // Function is for clearing the localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=> {
    console.log(user)
  }, [user]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
        headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(response => response.json())
    .then(data => {
        setUser({
            id: data._id,
            isAdmin: data.isAdmin 
        })
    })
  }, [])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
          <Container>
            <Routes>
              <Route path ='/' element = {<Home/>}/>
              <Route path ='/register' element = {<Register/>}/>
              <Route path = '/login' element = {<Login/>} />
              <Route path = '/logout' element = {<Logout/>} />
              <Route path = '/products' element = {<Products/>} />
              <Route path = '/products/archive' element = {<Products/>} />
              <Route path = '/products/:id' element = {<ProductView/>}/>
              <Route path = '/cart' element = {<Cart/>}/>
              <Route path = '/orders' element = {<Orders/>}/>
              <Route path="*" element= {<Undefined/>} />
            </Routes>
        </Container>
      </BrowserRouter>
    </UserProvider>
  );
}
export default App;