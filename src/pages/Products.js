import { Fragment, useState, useEffect, useContext } from "react";
import {Row, Col, Table, Div, Container } from 'react-bootstrap';
import AddProductCard from "../components/AddProductCard";
import AllProductsAdmin from "../components/AllProductsAdmin";
import ArchivedProduct from "../components/ArchivedProducts";
import AllProductsUser from "../components/AllProductsUser";


import UserContext from '../UserContext';

export default function Products(){

    const [products, setProducts] = useState([]);
    const [activeTab, setActiveTab] = useState("active");

    const { user, setUser} = useContext(UserContext);

    //Gets the product data when page is refreshed
    useEffect(() => {
        fetchProducts();
    }, [activeTab]);

    // Fetch products when the 'products' state changes
  useEffect(() => {
    fetchProducts();
  }, [products]);

    const fetchProducts = () => {
        let fetchUrl;
        
        if (user.isAdmin) {
            fetchUrl =
              activeTab === "active"
                ? `${process.env.REACT_APP_API_URL}/products/`
                : `${process.env.REACT_APP_API_URL}/products/archive`;
          } else {
            fetchUrl = `${process.env.REACT_APP_API_URL}/products/`;
          }

        fetch(fetchUrl)
            .then((response) => response.json())
            .then(data => {
                setProducts(data);
            })
            .catch(error => console.error("Error fetching products:", error));
     };


    //Sets Nav link tab status
    const handleTabChange = (tab) => {
        setActiveTab(tab);
      };
    
    return (
        <Fragment>
            {user.isAdmin === true ? (
            <Container fluid style={{ paddingTop: '40px' }}>
                <Row>
                    <Col md={6} lg={4}>
                        <AddProductCard />
                    </Col>
                    <Col md={12} lg={8}>
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a className={`nav-link ${activeTab === "active" ? "active" : ""}`}
                            onClick={() => handleTabChange("active")}>Products</a>
                        </li>
                        <li class="nav-item">
                            <a className={`nav-link ${activeTab === "archive" ? "active" : ""}`}
                            onClick={() => handleTabChange("archive")}>Archived</a>
                        </li>
                    </ul>
                        <div className="table-responsive" style={{ paddingTop: '20px' }}>
                        <Table className="table-bordered">
                            <thead className="table-light">
                            <tr>
                                <th scope="col">Product Id</th>
                                <th scope="col">Image</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Product Description</th>
                                <th scope="col">Price</th>
                                <th scope="col">Update Product</th>
                            </tr>
                            </thead>
                            <tbody className="table-group-divider">
                            {
                            activeTab === "active" 
                            ? 
                            (
                                products.map((product) => (
                                <AllProductsAdmin key={product._id} productProp={product} />)
                            
                                ))
                            :
                            (
                                products.map((product) => (
                                <ArchivedProduct key={product._id} productProp={product} />)
                                ))
                            }
                            </tbody>
                        </Table>
                        </div>
                    </Col>
                </Row>
            </Container>)
            : (
                <Container fluid className="products-container">
                    <Row style={{ paddingTop: '20px' }}>
                    {products.map((product) => (
                        <Col key={product._id} xs={12} sm={6} md={4} lg={4} xl={3} style={{ paddingTop: '20px' }}>
                            <AllProductsUser productProp={product} />
                        </Col>
                     ))}
                    </Row>
                </Container>
            )
                        }
        </Fragment>
);
}