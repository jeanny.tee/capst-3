import { Fragment } from "react";
import Banner from "../components/Banner";
import Carousel from 'react-bootstrap/Carousel';

export default function Home () {
    return (
        
        <Fragment>
            <Carousel fade className="carousel">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://cdn.zappy.app/436670c99a79440563eae5e21eff0eb1.png"
          alt="First slide"
        />
        <Carousel.Caption style={{ color: 'black' }}>
          <h3>Premium Pastries</h3>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://cdn.zappy.app/a0bbfd0e6cd16f4b314efbedbfe1da1e.png"
          alt="Second slide"
        />

        <Carousel.Caption style={{ color: 'black' }}>
          <h3>Buttery Brioche Buns</h3>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://cdn.zappy.app/cd587e2e716d6a0d684f9ac5287a3d96.png"
          alt="Third slide"
        />

        <Carousel.Caption style={{ color: 'white' }}>
          <h3>Elevate Your Menu</h3>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
            <Banner />
        </Fragment>
    )
}