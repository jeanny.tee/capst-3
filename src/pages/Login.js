import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

//sweetalert2 is a simple and useful package for generating user allerts with ReactJS
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {

    const navigate = useNavigate();
    
    /// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
	const [isPassed, setIsPassed] = useState(true);
    const [isDisabled, setIsDisabled] = useState(true);
	
    // Allows us to consume the UserContext object and it's properties to use for user validation
    const { user, setUser} = useContext(UserContext);

    // const [user, setUser] = useState(localStorage.getItem('email'));

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => {
            // Check for a 404 response
            if (response.status === 404) {
                // Handle user not found error
                Swal2.fire({
                    title: "Login unsuccessful!",
                    icon: 'error',
                    text: 'User does not exist. Please register to create an account.'
                });
            } else if (!response.ok) {
                // Handle other non-2xx status codes as general errors
                Swal2.fire({
                    title: "Error",
                    icon: 'error',
                    text: 'An error occurred. Please try again.'
                });
            } else {
                // If the response is ok, continue processing
                return response.json();
            }
        })
        .then(data => {
            if (data) {
                if(data.access) {
                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);
        
                    Swal2.fire({
                        title: 'Login successful!',
                        icon: 'success',
                        text: 'Welcome to Breadstand!'
                    });
        
                    navigate('/');
                } else if(data.error) {
                    // Handle other types of responses (e.g., errors)
                    Swal2.fire({
                        title: data.error,
                        icon: 'error',
                    });
                } else {
                    // Handle unexpected data structure
                    console.error('Unexpected data:', data);
                }
            } else {
                // Handle no data (e.g., a null or undefined response)
                console.error('No data:', data);
            }
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin 
            })
        })

    }

    useEffect(()=>{
		if(email.length < 8){
			setIsPassed(false);
		}else{
			setIsPassed(true);
		}
	}, [email]);
 

	//this useEffect will disable or enable our sign up button
	useEffect(()=> {
		//we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
		if(email !== '' && password !== '' ){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [email, password]);


	return (
        user.id === null || user.id ===undefined
        ?
            <Row style={{ paddingTop: '50px' }}>
                <Col className = 'col-6 mx-auto'>
                    <h1 className = 'text-center mt-2'>Login</h1>
                    <Form onSubmit={(e) => authenticate(e)}>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted" hidden = {isPassed}>
				            The email should at least be 8 characters!
				            </Form.Text>

                        </Form.Group>

                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>
                        
                        <Button variant="success" type="submit" id="submitBtn" disabled = {isDisabled} className ='mt-3'>
                                Login
                            </Button>
                        
                    </Form>
                </Col>
            </Row>

        :
          <Navigate to = '*' />  
    )
}

