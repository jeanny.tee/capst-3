import {Button, Form, Row, Col} from 'react-bootstrap';
//we need to import the useState from the react
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

export default function Register(){
	const navigate = useNavigate();
	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isPassed, setIsPassed] = useState(true);
	const [mobileIspassed, setMobileIsPassed] = useState(true);
	const [isDisabled, setIsDisabled] = useState(true);

	const { user, setUser} = useContext(UserContext);

    //we are going to add/create a state that will declare whether the password1 and password 2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true); 

 
 	//Mobile number requirement help text
	useEffect(()=>{
		if(email.length < 8){
			setIsPassed(false);
		}else{
			setIsPassed(true);
		}
	}, [email]);

	//Mobile number requirement help text
	useEffect(()=>{
		if(mobileNo.length < 11){
			setMobileIsPassed(false);
		}else{
			setMobileIsPassed(true);
		}
	}, [mobileNo]);

	//this useEffect will disable or enable our sign up button
	useEffect(()=> {
		//we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
		if(firstName !== '' && lastName !== '' && mobileNo !== '' && mobileNo.length >= 11 && email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && email.length >= 8){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [firstName, lastName, mobileNo, email, password1, password2]);

	//function to simulate user registration
	function registerUser(event) {
		//prevent page reloading
		event.preventDefault();
		
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
				email: `${email}`,
			})
		})
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			if(data === true){
				Swal2.fire({
					title: 'User email is registered',
					icon: 'error',
					text: 'Please login with your existing account!'
				})
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
					method: "POST",
					headers: {
						'Content-Type' : 'application/json',
					},
					body: JSON.stringify({
						firstName: `${firstName}`,
						lastName: `${lastName}`,
						email: `${email}`,
						mobileNo: `${mobileNo}`,
						password: `${password1}`,
					})
				})
				.then(response => response.json())
				.then(data => {
					if(data === true){
						Swal2.fire({
							title: 'Registration Successful',
							icon: 'success',
							text: 'Please login!'
						})
						navigate('/login');
					}else {
						Swal2.fire({
							title: 'Registration unsuccessful',
							icon: 'error',
							text: 'Please try again!'
						})
					}
				})
			}
		})

		

		// alert('Thank you for registering!');
		// navigate('/login');

		// setEmail('');
		// setPassword1('');
		// setPassword2('');*/
}

    //useEffect to validate whether the password1 is equal to password2
	useEffect(() => {
		if(password1 !== password2){
			setIsPasswordMatch(false);
		}else{
			setIsPasswordMatch(true);
		}

	}, [password1, password2]);

	return(
		user.id === null || user.id ===undefined
        ?
			<Row style={{ paddingTop: '50px' }}>
				<Col className = "col-6 mx-auto">
					<h1 className = "text-center">Register</h1>
					<Form onSubmit ={event => registerUser(event)}>
						<Form.Group className="mb-3" controlId="formBasicFirstName">
							<Form.Label>First Name</Form.Label>
							<Form.Control 
								type="string" 
								placeholder="Enter your first name here" 
								value = {firstName}
								onChange = {event => setFirstName(event.target.value)}
								/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formBasicLastName">
							<Form.Label>Last Name</Form.Label>
							<Form.Control 
								type="string" 
								placeholder="Enter your last name here" 
								value = {lastName}
								onChange = {event => setLastName(event.target.value)}
								/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formBasicMobileNo">
							<Form.Label>Contact Number</Form.Label>
							<Form.Control 
								type="string" 
								placeholder="Enter your mobile number" 
								value = {mobileNo}
								onChange = {event => setMobileNo(event.target.value)}
								/>
							<Form.Text className="text-muted" hidden = {mobileIspassed}>
							The contact number should at least be 11 digits!
							</Form.Text>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control 
								type="email" 
								placeholder="Enter email" 
								value = {email}
								onChange = {event => setEmail(event.target.value)}
								/>
							<Form.Text className="text-muted" hidden = {isPassed}>
							The email should at least be 8 characters!
							</Form.Text>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword1">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Password" 
								value = {password1}
								onChange = {event => setPassword1(event.target.value)}
								/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword2">
							<Form.Label>Confirm Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Retype your nominated password" 
								value = {password2}
								onChange = {event => setPassword2(event.target.value)}
								/>
								<Form.Text className="text-danger" hidden = {isPasswordMatch}>
							The passwords does not match!
								</Form.Text>

						</Form.Group>

						

						<Button variant="success" type="submit" disabled = {isDisabled}>
							Sign up
						</Button>
					</Form>
				</Col>
			</Row>
		:
		<Navigate to = '*' />  
		)
}
