import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function NotFound() {
    return (
        <Row>
            <Col className="p-5">
            <h1>404 - Page Not Found</h1>
            <p>The page you are looking for is not found.</p>
            <Button as = {Link} to = '/' variant="primary" >Back to Home</Button>
            </Col>
        </Row>
    )
}