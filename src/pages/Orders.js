import { Fragment, useState, useEffect, useContext } from "react";
import {Row, Col, Table, Div, Container } from 'react-bootstrap';
import AllProductsUser from "../components/AllProductsUser";
import MyOrders from "../components/MyOrders";


import UserContext from '../UserContext';

export default function Orders(){

    const [orders, setOrders] = useState([]);
    const [activeTab, setActiveTab] = useState("active");

    const { user, setUser} = useContext(UserContext);

     //Gets the product data when page is refreshed
     useEffect(() => {
        fetchOrders();
    }, [activeTab]);

    const fetchOrders = () => {

        if (user.isAdmin){
            const url = `${process.env.REACT_APP_API_URL}/users/orders`;
          
            // Append the query parameter 'status' based on the activeTab value
            const queryParam = activeTab === "active" ? "fulfilled=false" : "fulfilled=true";
            const fullUrl = `${url}?${queryParam}`;
          
            fetch(fullUrl, {
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
              }
            })
              .then((response) => response.json())
              .then(data => {
                // console.log(data);
                setOrders(data);
              })
              .catch(error => console.error("Error fetching products:", error));
          }else { fetch(`${process.env.REACT_APP_API_URL}/users/myorders`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
          })
            .then((response) => response.json())
            .then(data => {
                // console.log(data)
                setOrders(data);
            })
            .catch(error => console.error("Error fetching products:", error));}
    //Gets the Order data when page is refreshed
       
    }

     //Sets Nav link tab status
     const handleTabChange = (tab) => {
        setActiveTab(tab);
      };

    
    return (
        <div>
            {user.isAdmin === true ? (
            <Container fluid style={{ paddingTop: '40px' }}>
                <Row>
                    <Col>
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a className={`nav-link ${activeTab === "active" ? "active" : ""}`}
                            onClick={() => handleTabChange("active")}>Orders</a>
                        </li>
                        <li class="nav-item">
                            <a className={`nav-link ${activeTab === "archive" ? "active" : ""}`}
                            onClick={() => handleTabChange("fulfilled")}>Fulfilled</a>
                        </li>
                    </ul>
                        <div className="table-responsive">
                        <Table className="table-bordered">
                            <thead className="table-light">
                            <tr>
                                <th scope="col">Order Id</th>
                                <th scope="col">Products</th>
                                <th scope="col">Purchased Date</th>
                                <th scope="col">Total Amount</th>
                                <th scope="col">Order Status</th>
                                <th scope="col">Update Status</th>
                            </tr>
                            </thead>
                            <tbody className="table-group-divider">
                            {
                            activeTab === "active" 
                            ? 
                            (
                                orders.map((order) => (
                                    <MyOrders key={order._id}  orderProp={order} />)
                                ))
                            :
                            (
                                orders.map((order) => (
                                    <MyOrders key={order._id}  orderProp={order} />)
                                ))
                            }
                            </tbody>
                        </Table>
                        </div>
                    </Col>
                </Row>
            </Container>)
            : (
                <Container fluid className="orders-container">
                    <Row>
                        <h1>My Orders</h1>
                    </Row>
                <div className="table-responsive">
                <Table className="table-bordered">
                  <thead className="table-light">
                  <tr>
                      <th scope="col">Order Id</th>
                      <th scope="col">Products</th>
                      <th scope="col">Purchased Date</th>
                      <th scope="col">Total Amount</th>
                      <th scope="col">Order Status</th>
                  </tr>
                  </thead>
                    <tbody className="table-group-divider">
                    {orders.map((order) => (
                        <MyOrders key={order._id}  orderProp={order} />)
                    )}
                    </tbody>
                </Table>
                </div>
                
                </Container>
             )
            } 
        </div>
);
}