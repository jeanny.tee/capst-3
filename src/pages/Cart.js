import { useState, useEffect, useContext } from "react";
import {Row, Col, Table, Div, Container, Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate, useParams } from 'react-router-dom';

import Swal2 from 'sweetalert2';

import AddToCart from "../components/AddToCart";
import NoItemsInCart from "../components/NoItemsInCart";
import AllProductsUser from "../components/AllProductsUser";


import UserContext from '../UserContext';

export default function Cart(){
   
    const [cart, setCart] = useState({});
    const [products, setProducts] = useState([]);
    const { user, setUser} = useContext(UserContext);
    const [specialInstructions, setSpecialinstructions] = useState('');
    const navigate = useNavigate();


    // Function to fetch the cart items

     useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/cart`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
          })
            .then((response) => response.json())
            .then(data => {
              setCart(data);
            })
            .catch(error => console.error("Error fetching cart items:", error));
      }, [])
      
 
  // Function to fetch the products

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then((response) => response.json())
        .then(data => {
          setProducts(data);
        })
        .catch(error => console.error("Error fetching products:", error));

  }, [])

   //function to process Checkout and create a job order
   function checkOut(event) {
    //prevent page reloading
    event.preventDefault();
    console.log(cart.products)
    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout/`,{
            method: "POST",
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    products: cart.products,
                })
                
            })
            
            .then(response => {
                // Log the response to see the data structure
                console.log(response);
                return response.json();})
            .then(data => {
                console.log(data);
                if(data === true){
                    Swal2.fire({
                        title: 'Thank you for your order!',
                        icon: 'success',
                    })
                    navigate('/orders');
                }else {
                    Swal2.fire({
                        title: 'Unable to process order',
                        icon: 'error',
                        text: 'Please try again!'
                    })
                }
            })
        }
    
      return (
        <div>
          {/* Check if user is authenticated */}
          {user.id !== null || user.id !== undefined ? (
            // Check if user has existing items in the cart
            cart.length === 0 ? (
              <>
                <NoItemsInCart />
                {console.log(products)} 
                <Container fluid className="products-container">
                    <Row>
                    {products.map((product) => (
                        <Col key={product._id} xs={12} sm={6} md={4} lg={4} xl={3}>
                            <AllProductsUser productProp={product} />
                        </Col>
                     ))}
                    </Row>
                </Container>
              </>
            ) : (
              <>
              <div className="table-responsive">
              <Table className="table-bordered">
                  <thead className="table-light">
                  <tr>
                      <th scope="col">Product Id</th>
                      <th scope="col">Image</th>
                      <th scope="col">Product Name</th>
                      <th scope="col">Product Description</th>
                      <th scope="col">Price</th>
                      <th scope="col">Quantity</th>
                      <th scope="col">Update Product</th>
                  </tr>
                  </thead>
                  <tbody className="table-group-divider">
                  {cart.products?.map((product) => (
                        <AddToCart key={product._id} productProp={product} />
                    ))}
                    </tbody>
                    </Table>
                    </div>
                <div style={{ textAlign: 'right' }}>
                    <h3>Total Amount: {cart.totalAmount}</h3>
                </div>
                <Form onSubmit ={event => checkOut(event)}>
                <Button variant="primary" type="submit">
                            Checkout
						</Button>

                 </Form>
                {/* <Row>
				    <Col className = "col-6 mx-auto">
					    <Form onSubmit ={event => checkOut(event)}>
                            <Form.Group className="mb-3" controlId="formBasicOrder">
							<Form.Label>Special Instructions</Form.Label>
							<Form.Control 
								type="Text" 
								placeholder="Enter Special Instructions here!" 
								value = {specialInstructions}
								onChange = {event => setSpecialinstructions(event.target.value)}
								/>
				        </Form.Group>
                        <Button variant="primary" type="submit">
                            Checkout
						</Button>
                        </Form>
                    </Col>
                </Row> */}
              </>
            )
          ) : (
            <>
              <NoItemsInCart />
              <Container fluid className="products-container">
                    <Row>
                    {products.map((product) => (
                        <Col key={product._id} xs={12} sm={6} md={4} lg={4} xl={3}>
                            <AllProductsUser productProp={product} />
                        </Col>
                     ))}
                    </Row>
                </Container>
            </>
          )}
        </div>
      );
};
            
                

