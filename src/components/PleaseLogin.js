import { useState } from 'react';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

function PleaseLoginAlert () {
  const [show, setShow] = useState(true);

  return (
    <>
      <Alert show={show} variant="success">
        <Alert.Heading>Please Login!</Alert.Heading>
        <p>
          Please Log in to add items to your cart!
        </p>
        <hr />
        <div className="d-flex justify-content-end">
          <Button onClick={() => (false)} variant="outline-success">
            Okay!
          </Button>
        </div>
      </Alert>

      {!show && <Button onClick={() => setShow(true)}>Show Alert</Button>}
    </>
  );
}

export default PleaseLoginAlert;