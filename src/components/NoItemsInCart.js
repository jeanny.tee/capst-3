import {Row, Col, Button, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function NoItemsInCart() {
  return (
    <Row>
		<Col>
            <Card>
                <Card.Header>Shopping Cart!</Card.Header>
                <Card.Body>
                <Card.Title>There are no items in your cart!</Card.Title>
                <Card.Text>
                    Please browse through our menu.
                </Card.Text>
                </Card.Body>
            </Card>
        </Col>
  </Row>
  );
}
