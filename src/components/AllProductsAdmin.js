import {Row, Col, Button, Table} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

import { Navigate, useNavigate, useParams } from 'react-router-dom';

export default function AllProductsAdmin({productProp}) {

    const {_id, name, description, price, image} = productProp;

    const [isEditing, setIsEditing] = useState(false);
    const [udpatedImage, setUpdatedImage] = useState(image);
    const [updatedName, setUpdatedName] = useState(name);
    const [updatedDescription, setUpdatedDescription] = useState(name);
    const [updatedPrice, setUpdatedPrice] = useState(name);
    const navigate = useNavigate();

    
    //Changes the view to edit mode
    const editProduct = () => {
        setIsEditing(true);
    };

    //Goes back to view mode when cancel button is clicked
    const cancelEdit = () => {
        setIsEditing(false);
        // Reset the edited values to the original values
        setUpdatedImage(image);
        setUpdatedName(name);
        setUpdatedDescription(description);
        setUpdatedPrice(price);
      };

    //listens to changes in the isEditing state.
    useEffect(() => {
    // If isEditing is false, it means the update is successful, so reset the state with updated values
    if (!isEditing) {
        setUpdatedImage(image);
        setUpdatedName(name);
        setUpdatedDescription(description);
        setUpdatedPrice(price);
    }
}, [isEditing]);

    //Function to update the Product
    const updateProduct = () => {
        console.log(udpatedImage, updatedName, updatedDescription, updatedPrice)
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`,{
				method: "PUT",
				headers: {
					'Content-Type' : 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
                        image: `${udpatedImage}`,
						name: `${updatedName}`,
						description: `${updatedDescription}`,
						price: `${updatedPrice}`,
					})
				})
				.then(response => response.json())
				.then(data => {
					if(data === true){
						alert("Product updated successfully!");
                        setIsEditing(false);
					}else {
						alert("Failed to update product. Please try again.")
					}
				})
    }
    //Function to Archive a product
    const archiveProduct = () => {
        console.log(udpatedImage, updatedName, updatedDescription, updatedPrice)
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`,{
				method: "PUT",
				headers: {
					'Content-Type' : 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
                        image: `${udpatedImage}`,
						name: `${updatedName}`,
						description: `${updatedDescription}`,
						price: `${updatedPrice}`,
					})
				})
				.then(response => response.json())
				.then(data => {
					if(data === true){
						alert("Product Archived");
					}else {
						alert("Failed to archive product. Please try again.")
					}
				})

    }
    // const productImage = <img src={image} alt={name} style={{ maxWidth: '100px' }} />;
    return (
       
        <tr>
            <th scope="row">{_id}</th>
            <td>
                {/* editing view */}
                {isEditing ? (
                <input
                    type="text"
                    value={udpatedImage}
                    onChange={(e) => setUpdatedImage(e.target.value)}
                />
                ) : (
                    <img src={udpatedImage} alt={name} style={{ maxWidth: '100px' }} />
                )}
            </td>
            <td>
                {isEditing ? (
                <input
                    type="text"
                    value={updatedName}
                    onChange={(e) => setUpdatedName(e.target.value)}
                />
                ) : (
                    updatedName
                )}
            </td>
            <td>
                {isEditing ? (
                <input
                    type="text"
                    value={updatedDescription}
                    onChange={(e) => setUpdatedDescription(e.target.value)}
                />
                ) : (
                    updatedDescription
                )}
            </td>
            <td>
                {isEditing ? (
                <input
                    type="number"
                    value={updatedPrice}
                    onChange={(e) => setUpdatedPrice(e.target.value)}
                />
                ) : (
                    updatedPrice
                )}
            </td>
            <td>
                {isEditing ? (
                <>
                <Button variant="success" onClick={updateProduct}>Submit Changes</Button>
                <Button variant="success" onClick={cancelEdit}>Cancel</Button>
                </>
                ) : (
                    <>
                    <Button variant="success" onClick={editProduct}>Edit</Button>
                    <Button variant="success" onClick={archiveProduct}>Archive</Button>
                    </>
            )}
            </td>
        </tr>
    );

}