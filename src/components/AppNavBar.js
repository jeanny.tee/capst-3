import { Navbar, Nav, Container, Image } from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';
import {NavLink, Link} from 'react-router-dom';

import UserContext from '../UserContext';

// The as keyword allows components to be treated as if they are different component gaining access to it's properties and functionalities
//The to keyword is used in place of the 'href' for providing the URL for the page
export default function AppNavBar() {

	const { user } = useContext(UserContext);


	return (
		<Navbar className="custom-navbar" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as = {Link} to = '/'>
				<Image
					src="https://cdn.zappy.app/011fe01c09b2ae70b0ba5a87c5c0f49b.png"
					alt="The Bread Stand"
					width="80" 
					height="auto"
					style={{ paddingLeft: '10px', paddingTop: '2px' }}
					/>
				</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
		            <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
		            
		            {
		            	user.id === null || user.id ===undefined
		            	?
		            	<>
		            		<Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
		            		<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
		            	</>
		            	:
						<>
						{	
							user.isAdmin === true ?
							<>
							<Nav.Link as = {NavLink} to = '/orders'>Orders</Nav.Link>
							<Nav.Link as = {NavLink} to = '/users'>Users</Nav.Link>
							<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
							</>
							:
							<>
							<Nav.Link as = {NavLink} to = '/orders'>Orders</Nav.Link>
							<Nav.Link as = {NavLink} to = '/cart'>My Cart</Nav.Link>
							<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
							</>
						}
						</>
		            	}	
		            	
						
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>

	)
}