import { Button, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';


export default function Banner() {
    return (
        <Row>
            <Col className="p-5">
            <h1>The Bread Stand</h1>
            <p>Wholesale pastries below the market price yet guaranteed high quality to help boost SME sale!</p>
            <Button as = {Link} to = '/products' variant="success">View our Products!</Button>
            </Col>
        </Row>
    )
}