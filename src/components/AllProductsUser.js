import {Row, Col, Button, Card, } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect, useRef } from 'react';
import {Link} from 'react-router-dom';

import { Navigate, useNavigate, useParams } from 'react-router-dom';

export default function AllProductsUser({productProp}) {
    const {_id, name, description, price, image} = productProp;

    // Use a ref to keep track of the maximum card height
  const cardRef = useRef(null);

  useEffect(() => {
    const cards = document.getElementsByClassName("user-card");
    let maxHeight = 0;

    for (let i = 0; i < cards.length; i++) {
      const cardHeight = cards[i].clientHeight;
      if (cardHeight > maxHeight) {
        maxHeight = cardHeight;
      }
    }

    cardRef.current.style.height = `${maxHeight}px`;
  }, []);

    return (

        <Card className="user-card" ref={cardRef}>
          <Card.Img variant="top" src={image} className="product-image"/>  
          <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Button as= {Link} to = {`/products/${_id}`} variant="success">View Product</Button>
          </Card.Body>
      </Card>
        
      )
}