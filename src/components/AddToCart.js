import {Row, Col, Button, Table, Form} from 'react-bootstrap';
import { Fragment, useState } from "react";
import PropTypes from 'prop-types';
import Swal2 from 'sweetalert2';
import { useParams } from 'react-router-dom';


export default function AddToCart({productProp}) {

    const {_id, productId, name, description, price, image, quantity} = productProp;
    const [specialInstructions, setSpecialinstructions] = useState('');
    
    //Function to remove the Product from Cart

    const removeProduct = () => {
        const {_id, name, productId, image, description, price,} = productProp;
        console.log(productProp)
		
		fetch(`${process.env.REACT_APP_API_URL}/cart/`,{
				method: "DELETE",
				headers: {
					'Content-Type' : 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
                        products: [productProp]
					})
				})
				.then(response => response.json())
				.then(data => {
					if(data === true){
						alert("Product Archived");
                        window.location.reload();
					}else {
						alert("Failed to archive product. Please try again.")
					}
				})

    }
   
    // const productImage = <img src={image} alt={name} style={{ maxWidth: '100px' }} />;
    return (

    <>
    <tr>
        <th>{productId}</th>
        <td><img src={image} alt={name} style={{ maxWidth: '100px' }} /></td>
        <td>{name}</td>
        <td>{description}</td>
        <td>{price}</td>
        <td>{quantity}</td>
        <td>
        <Button variant="primary" onClick={removeProduct}>Remove</Button>
        </td>
    </tr>
    </>  
    );

}