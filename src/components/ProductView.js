import {Row, Col, Button, Card, Container, Image, Placeholder} from 'react-bootstrap';
//the useParams allows us to get or extract the parameter included in our pages
import { useParams } from 'react-router-dom';

import {useEffect, useState} from 'react';
import Swal2 from 'sweetalert2';
import AddToCart from './AddToCart';

export default function ProductView(){

    const [image, setImage] = useState('');
	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');
	const [productId, setProductId] = useState('');

    const {id} = useParams();

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
                setImage(data.image);
				setName(data.name);
				setPrice(data.price);
				setDescription(data.description);
				setProductId(data._id)

		})

	}, [])

	const addTocart = (event) => {
		const products = {
			productId: productId,
			name: name,
			description: description,
			price: price,
			quantity: 1,
			image: image,
		  };
		fetch(`${process.env.REACT_APP_API_URL}/cart`,{
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				products: [products]})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data === true){
				//successful yung login
				Swal2.fire({
					title: 'Added to Cart!',
					icon: 'success',
					text: 'Proceed to the Orders page to checkout!'
				})
			}else{
				Swal2.fire({
					title: 'Please Login',
					icon: 'error',
					text: 'Please log in or register to purchase!'
				})
			}
		})


	}


	return(
    <Container style={{ paddingTop: '50px' }} className='card-container'>
		<Row>
			<Col xs={12} md={4} lg={6} className="d-flex align-items-center">
                <Image src={image} className="w-100" />
            </Col>
            <Col xs={12} md={8} lg={6}>
            <Card onSubmit ={event => addTocart(event)}>
                <Card.Header>{name}</Card.Header>
            <Card.Body>
                 
                <Card.Title>Description:</Card.Title>
				<Card.Text>Product Id:{productId}</Card.Text>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Button variant="success" onClick={addTocart}>Add to Cart</Button>
            </Card.Body>
            </Card>
			</Col>
		</Row>
    </Container>
    
    )
}
