import {Row, Col, Button, Form} from 'react-bootstrap';

import { Navigate, useNavigate, useParams } from 'react-router-dom';


import {useEffect, useState} from 'react';
import Swal2 from 'sweetalert2';

export default function AddProductCard(){
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState('');
    const navigate = useNavigate();

    //disabled button to make sure all fields are filled

    const [isDisabled, setIsDisabled] = useState(true);

    //this useEffect will disable or enable our sign up button
	useEffect(()=> {
		//we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
		if(name !== '' && description !== '' && price !== '' && image !== ''){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [name, description, price, image]);

    function addProduct(event) {
		//prevent page reloading
		event.preventDefault();
		
		fetch(`${process.env.REACT_APP_API_URL}/products/`,{
				method: "POST",
				headers: {
					'Content-Type' : 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						name: `${name}`,
						description: `${description}`,
						price: `${price}`,
						image: `${image}`,
					})
				})
				.then(response => response.json())
				.then(data => {
					if(data === true){
						Swal2.fire({
							title: 'Succesfully added a Product',
							icon: 'success',
						})
                        navigate('/products');
					}else {
						Swal2.fire({
							title: 'Unable to add Product',
							icon: 'error',
							text: 'Please try again!'
						})
					}
				})
			}
    return(
        <Row>
				<Col className = "mx-auto">
					<h2 className = "text-center">Add More Products</h2>
                    <p>Please fill out all product fields</p>
					<Form onSubmit ={event => addProduct(event)}>
						<Form.Group className="mb-3" controlId="formBasicProductName">
							<Form.Label>Product Name</Form.Label>
							<Form.Control 
								type="string" 
								placeholder="Enter the product name" 
								value = {name}
								onChange = {event => setName(event.target.value)}
								/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formBasicDescription">
							<Form.Label>Product Description</Form.Label>
							<Form.Control 
								type="string" 
								placeholder="Enter a product description" 
								value = {description}
								onChange = {event => setDescription(event.target.value)}
								/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formBasicPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="Number" 
								placeholder="Enter the price" 
								value = {price}
								onChange = {event => setPrice(event.target.value)}
								/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formBasicImage">
							<Form.Label>Product Image</Form.Label>
							<Form.Control 
								type="String" 
								placeholder="Enter a URL to the product image" 
								value = {image}
								onChange = {event => setImage(event.target.value)}
								/>
						</Form.Group>
						<Button variant="success" type="submit" disabled = {isDisabled}>
							Add Product
						</Button>
					</Form>
				</Col>
			</Row>
    )

}
