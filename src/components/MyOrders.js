import {Row, Col, Button, Table, Form} from 'react-bootstrap';
import { Fragment, useState } from "react";
import PropTypes from 'prop-types';
import Swal2 from 'sweetalert2';
import { useParams } from 'react-router-dom';

export default function MyOrders({orderProp}) {

    const {_id, products, totalAmount, purchasedOn, status} = orderProp;
    

   
    return (

    <>
    <tr>
        <th>{_id}</th>
        <td>{products.map((product) => (
            <div key={product.productId}>
              {product.name} - {product.quantity}
            </div>
          ))}</td>
        <td>{purchasedOn}</td>
        <td>{totalAmount}</td>
        <td>{status}</td>
    </tr>
    </>  
    );

}