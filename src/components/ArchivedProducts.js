import {Row, Col, Button, Table} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

import { Navigate, useNavigate, useParams } from 'react-router-dom';

export default function ArchivedProduct ({productProp}) {

    
    const {_id, name, description, price, stock, image} = productProp;

    const updateProduct = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`,{
        method: "PUT",
        headers: {
            'Content-Type' : 'application/json',
            'Authorization' : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive : true
            })
        })
        .then(response => response.json())
        .then(data => {
            if(data === true){
                alert("Product restored successfully!");
            }else {
                alert("Failed to restore product. Please try again.")
            }
        })
    }
    return (
        
                <tr>
                    <th scope="row">{_id}</th>
                    <td><img src={image} alt={name} style={{ maxWidth: '100px' }} /></td>
                    <td>{name}</td>
                    <td>{description}</td>
                    <td>{price}</td>
                    <td>
                    <Button variant="primary" onClick={updateProduct}>Unarchive</Button>
                    </td>
                </tr>

    )

}
